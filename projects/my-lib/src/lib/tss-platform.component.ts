import { Component, Input, OnInit, Output, TemplateRef, EventEmitter } from '@angular/core';
import { MenuService } from './menu.service';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { ReuseTabService } from './reuse-tab';

@Component({
  selector: 'tss-platform',
  templateUrl: './tss-platform.component.html',
  styleUrls: ['./tss-platform.component.less']
})
export class TssPlatformComponent implements OnInit {
  @Input() ntMenus: Menu[] = [];
  @Input() ntTitle: string;
  @Input() ntLogo: string;
  @Input() ntExtra: TemplateRef<{}>;
  @Input() ntToolsBars: TemplateRef<{}>;
  @Input() ntAvatar: TemplateRef<{}>;
  @Output() onCollapse: EventEmitter<boolean> = new EventEmitter<boolean>();
  isCollapsed = false;
  auth: any = {};
  fullScreen = false;

  constructor(
    private router: Router,
    private menuService: MenuService,
    private reuseSrv: ReuseTabService,
  ) {
    router.events.pipe(filter(e => e instanceof NavigationEnd)).subscribe(() => {
      this.fullScreen = false;
      this.ntMenus.length > 0 && this.setOpen(this.router.url.split('?')[0]);
    });
  }

  ngOnInit() {
    this.menuService.init(this.ntMenus, this, () => {
      this.menuService.getAuth().subscribe(ret => {
        this.auth = {};
        ret.map(e => {
          this.auth[e] = true;
        });
        this.setOpen(this.router.url.split('?')[0]);
      });
    });
  }

  clearReuse() {
    this.reuseSrv.clear();
  }

  hasSub(data: Menu) {
    if (data.children && data.children.length > 0 && data.children.some(e => !e.hidden)) {
      return true;
    } else {
      return false;
    }
  }

  collapse() {
    this.isCollapsed = !this.isCollapsed;
    this.onCollapse.emit(this.isCollapsed);
  }

  getwidth() {
    return window.innerWidth - (this.isCollapsed ? 80 : 200) + 'px';
  }

  fullScreenCtrl(menu) {
    this.fullScreen = menu.fullScreen ? true : false;
  }

  collapsedChange($event) {
    this.isCollapsed = $event;
    this.onCollapse.emit(this.isCollapsed);
  }

  setOpen(link: string) {
    let item: Menu = null;
    this.menuService.visit(this.ntMenus, i => {
      // 以下控制选中后, 无关菜单是否收起
      // i.url !== link && (i.open = false);
      i.url === link && (item = i);
      // if (i.url === link) {
      //   item = i;
      //   this.fullScreenCtrl(i);
      // }
      if (Object.keys(this.auth).length > 0) {
        if ((i.auth && !this.auth[i.auth]) || i.hidden) {
          i.authHidden = true;
        } else {
          i.authHidden = false;
        }
      }
    });
    if (item) {
      let pItem = item.__parent;
      while (pItem) {
        pItem.open = true;
        pItem = pItem.__parent;
      }
    }
  }
}

/**
 * @member title: 菜单标题, 也将会用于面包屑标题
 * @member url: 路由链接
 * @member icon: ng-zorro 图标类型
 * @member auth: 权限点
 * @member hidden: 隐藏菜单, 用于那些编辑页, 可以让组件识别编辑页归属于哪个菜单下, 用于面包屑
 */
export interface Menu {
  title: string;
  url?: string;
  icon?: string;
  children?: Menu[];
  open?: boolean;
  auth?: string;
  hidden?: boolean;
  authHidden?: boolean;
  __parent?: Menu;
  id?: number;
  fullScreen?: boolean;
  reuse?: boolean;
}
