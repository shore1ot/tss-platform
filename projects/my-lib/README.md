# TssPlatform

基于 Angular 和 ng-zorro-antd 的菜单组件
并添加配套路由复用策略

## 依赖

- Angular: 8.2.5
- ng-zorro-antd: 8.5.2

## 使用方式

按照如下步骤进行:

### 安装依赖

`npm install tss-platform`

### 在模块中引入菜单模块

以 `app.module.ts` 为例

```
import { TssPlatformModule } from 'tss-platform';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
  ],
  imports: [
    ...
    TssPlatformModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

### 使用菜单组件


```
import { Component } from '@angular/core';
import { Menu } from 'tss-platform';

@Component({
    selector: 'app-layout',
    template: `
        <tss-platform
                [ntMenus]="menus"
                [ntExtra]="extra"
                [ntAvatar]="avatar"
                [ntTitle]="'项目名称'"
                [ntLogo]="!collapse? 'assets/logo-full.png': 'assets/logo.png'"
                (onCollapse)="onCollapse($event)"
            >
                <ng-template #extra>
                    <!-- 菜单右上角操作块 -->
                </ng-template>

                <ng-template #avatar>
                    <!-- 菜单左侧头像块 -->
                </ng-template>
            </tss-platform>
    `,
})
export class LayoutComponent {

    menus: Menu[] = [
        {
            title: '一级菜单',
            url: '/link',
            icon: 'read',
            auth: 'authority',   // 权限点
            children: [
                {
                    title: '二级菜单-列表',
                    url: '/link/list',
                    hidden: true,   // 隐藏页面
                },
                {
                    title: '二级菜单-编辑',
                    url: '/link/edit',
                }
            ]
        }
    ];

    collapse = false;

    onCollapse($event: boolean): void {
        this.collapse = $event;
    }
}
```

```
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`
})
export class AppComponent {
}
```

### 路由配置

```
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'link',
        pathMatch: 'full'
      },
      {
        path: 'link',
        loadChildren: () => import('./xxxx.module').then((m) => m.XxxxxModule)
      },
    ]
  },
  {
    // 登录模块可跳出菜单
    path: 'passport',
    loadChildren: () => import('./passport.module').then((m) => m.PassportModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
```

### 权限注入

`MenuService.setAuth(['xxxx']);`

### 页面中使用面包屑

```
<tss-header></tss-header>
```

## 路由复用策略

需要缓存的页面菜单配置 `reuse: true` ，就可以在切换其它页面时缓存页面快照，再次返回该页面则还原上次页面信息，当点击菜单切换页面时清空页面复用缓存。并添加 `_onReuseInit` 和 `_onReuseDestroy` 钩子，只在路由复用页面进出时触发。具体使用方式如下:

### app.module.ts

```
import { ReuseTabService, ReuseTabStrategy } from 'tss-platform';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
  ],
  imports: [
    ...
  ],
  providers: [
    ...
    {
      provide: RouteReuseStrategy,
      useClass: ReuseTabStrategy,
      deps: [ReuseTabService],
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

### share.mudule.ts

```
import { ReuseTabModule } from 'tss-platform';
```

### 在需要复用的菜单中添加 reuse 参数

```
...
{
  title: '用户列表',
  url: '/user/edit',
  reuse: true,
},
```

注意: 点击菜单将自动清除所有复用缓存
