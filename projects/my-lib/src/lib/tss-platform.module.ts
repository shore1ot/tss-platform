import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { TssHeaderModule } from './tss-header/tss-header.module';
import { NZ_I18N, zh_CN } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import zh from '@angular/common/locales/zh';

import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { ReuseTabModule } from './reuse-tab';
import { TssPlatformComponent } from './tss-platform.component';

registerLocaleData(zh);

@NgModule({
  declarations: [
    TssPlatformComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    TssHeaderModule,
    NzLayoutModule,
    NzMenuModule,
    NzIconModule,
    ReuseTabModule,
  ],
  providers: [{ provide: NZ_I18N, useValue: zh_CN }],
  exports: [
    TssPlatformComponent,
    TssHeaderModule,
  ]
})
export class TssPlatformModule { }
