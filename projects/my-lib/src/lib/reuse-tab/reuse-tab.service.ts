import { Injectable, Injector, OnDestroy } from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
} from '@angular/router';
import { Unsubscribable } from 'rxjs';
import { MenuService } from '../menu.service';
import { ReuseTabCached, ReuseTitle } from './reuse-tab.interfaces';

/**
 * 路由复用类，提供复用所需要一些基本接口
 */
@Injectable({ providedIn: 'root' })
export class ReuseTabService implements OnDestroy {
  private _inited = false;
  private _cached: ReuseTabCached[] = [];
  private _titleCached: { [url: string]: ReuseTitle } = {};
  private _router$: Unsubscribable;

  private get snapshot() {
    return this.injector.get(ActivatedRoute).snapshot;
  }

  // #region public

  get inited() {
    return this._inited;
  }

  /** 当前路由地址 */
  get curUrl() {
    return this.getUrl(this.snapshot);
  }

  /** 获取已缓存的路由 */
  get items(): ReuseTabCached[] {
    return this._cached;
  }

  /** 获取指定路径缓存所在位置，`-1` 表示无缓存 */
  index(url: string): number {
    return this._cached.findIndex(w => w.url === url);
  }

  /** 获取指定路径缓存 */
  get(url?: string): ReuseTabCached | null {
    return url ? this._cached.find(w => w.url === url) || null : null;
  }

  /**
   * 清除所有缓存
   */
  clear() {
    this._cached = [];
  }

  /**
   * 获取标题
   *
   * @param url 指定URL
   * @param route 指定路由快照
   */
  getTitle(url: string, route?: ActivatedRouteSnapshot): ReuseTitle {
    if (this._titleCached[url]) {
      return this._titleCached[url];
    }

    if (route && route.data && route.data.title) {
      return {
        text: route.data.title,
      } as ReuseTitle;
    }

    const menu = this.getMenu(url);
    return menu ? { text: menu.title } : { text: url };
  }

  getTruthRoute(route: ActivatedRouteSnapshot) {
    let next = route;
    while (next.firstChild) {
      next = next.firstChild;
    }
    return next;
  }

  /**
   * 根据快照获取URL地址
   */
  getUrl(route: ActivatedRouteSnapshot): string {
    let next = this.getTruthRoute(route);
    const segments: string[] = [];
    while (next) {
      segments.push(next.url.join('/'));
      next = next.parent!;
    }
    const url =
      '/' +
      segments
        .filter(i => i)
        .reverse()
        .join('/');
    return url;
  }

  /**
   * 检查快照是否允许被复用
   */
  can(route: ActivatedRouteSnapshot): boolean {
    const url = this.getUrl(route);

    if (route.data && typeof route.data.reuse === 'boolean') {
      return route.data.reuse;
    }

    const menu = this.getMenu(url);
    if (!menu) {
      return false;
    }
    return menu.reuse;
  }

  constructor(private injector: Injector, private menuService: MenuService) { }

  init() {
    this._inited = true;
  }

  private getMenu(url: string) {
    const menus = this.menuService.getPathByUrl(url);
    if (!menus || menus.length === 0) {
      return null;
    }
    return menus.pop();
  }

  private runHook(method: string, _url: string, comp: any) {
    if (comp.instance && typeof comp.instance[method] === 'function') {
      comp.instance[method]();
    }
  }

  private hasInValidRoute(route: ActivatedRouteSnapshot) {
    return !route.routeConfig || route.routeConfig.loadChildren || route.routeConfig.children;
  }

  /**
   * 决定是否允许路由复用，若 `true` 会触发 `store`
   */
  shouldDetach(route: ActivatedRouteSnapshot): boolean {
    if (this.hasInValidRoute(route)) {
      return false;
    }
    return this.can(route);
  }

  /**
   * 存储
   */
  store(_snapshot: ActivatedRouteSnapshot, _handle: any) {
    const url = this.getUrl(_snapshot);
    const idx = this.index(url);

    const item: ReuseTabCached = {
      title: this.getTitle(url, _snapshot),
      url,
      _snapshot,
      _handle,
    };
    if (idx === -1) {
      this._cached.push(item);
    } else {
      this._cached[idx] = item;
    }

    if (_handle && _handle.componentRef) {
      this.runHook('_onReuseDestroy', url, _handle.componentRef);
    }
  }

  /**
   * 决定是否允许应用缓存数据
   */
  shouldAttach(route: ActivatedRouteSnapshot): boolean {
    if (this.hasInValidRoute(route)) {
      return false;
    }
    const url = this.getUrl(route);
    const data = this.get(url);
    const ret = !!(data && data._handle);
    if (ret && data!._handle.componentRef) {
      this.runHook('_onReuseInit', url, data!._handle.componentRef);
    }
    return ret;
  }

  /**
   * 提取复用数据
   */
  retrieve(route: ActivatedRouteSnapshot): {} | null {
    if (this.hasInValidRoute(route)) {
      return null;
    }
    const url = this.getUrl(route);
    const data = this.get(url);
    const ret = (data && data._handle) || null;
    return ret;
  }

  /**
   * 决定是否应该进行复用路由处理
   */
  shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
    let ret = future.routeConfig === curr.routeConfig;
    if (!ret) {
      return false;
    }

    const path = ((future.routeConfig && future.routeConfig.path) || '') as string;
    // tslint:disable-next-line: no-bitwise
    if (path.length > 0 && ~path.indexOf(':')) {
      const futureUrl = this.getUrl(future);
      const currUrl = this.getUrl(curr);
      ret = futureUrl === currUrl;
    }
    return ret;
  }

  ngOnDestroy(): void {
    const { _router$ } = this;
    this.clear();
    _router$ && _router$.unsubscribe();
  }
}
