/**
 * @member title: 菜单标题, 也将会用于面包屑标题
 * @member url: 路由链接
 * @member icon: ng-zorro 图标类型
 * @member auth: 权限点
 * @member hidden: 隐藏菜单, 用于那些编辑页, 可以让组件识别编辑页归属于哪个菜单下, 用于面包屑
 */
export interface Menu {
    title: string;
    url?: string;
    icon?: string;
    children?: Menu[];
    open?: boolean;
    auth?: string;
    hidden?: boolean;
    authHidden?: boolean;
    __parent?: Menu;
    id?: number;
    fullScreen?: boolean;
    reuse?: boolean;
}
