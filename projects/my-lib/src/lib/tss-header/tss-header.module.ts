import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { TssHeaderComponent } from './tss-header.component';
import { NZ_I18N, zh_CN } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import zh from '@angular/common/locales/zh';

import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';

registerLocaleData(zh);

@NgModule({
  declarations: [
    TssHeaderComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NzBreadCrumbModule,
  ],
  providers: [{ provide: NZ_I18N, useValue: zh_CN }],
  exports: [
    TssHeaderComponent
  ]
})
export class TssHeaderModule { }
