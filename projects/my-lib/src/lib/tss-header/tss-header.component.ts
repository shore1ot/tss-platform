import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { MenuService } from '../menu.service';

@Component({
  selector: 'tss-header',
  templateUrl: './tss-header.component.html',
  styleUrls: ['./tss-header.component.less']
})
export class TssHeaderComponent implements OnInit, OnChanges, OnDestroy {
  /**
   * 是否初始化
   */
  private inited = false;
  /**
   * 取消订阅
   */
  private unsubscribe$ = new Subject<void>();
  /**
   * 面包屑路径
   */
  path: any[] = [];
  /**
   * 操作区域
   */
  @Input() actions: TemplateRef<{}>

  private get menus() {
    if (this.path) {
      return this.path;
    }
    this.path = this.menuSrv.getPathByUrl(this.router.url.split('?')[0]);

    return this.path;
  }

  paths: PageHeaderPath[] = [];

  constructor(
    private router: Router,
    private menuSrv: MenuService,
  ) {
    this.menuSrv.checkInit().subscribe(init => {
      if (init) {
        this.path = null;
        this.refresh();
      }
    });
  }

  refresh() {
    this.genBreadcrumb();
  }

  private genBreadcrumb() {
    const paths: PageHeaderPath[] = [];
    this.menus.forEach(item => {
      const title = item.title;
      paths.push({ title, link: (item.url && [item.url]) as string[] });
    });
    this.paths = paths;
    return this;
  }

  ngOnInit() {
    this.refresh();
    this.inited = true;
  }

  ngOnChanges(): void {
    if (this.inited) {
      this.refresh();
    }
  }

  ngOnDestroy(): void {
    const { unsubscribe$ } = this;
    unsubscribe$.next();
    unsubscribe$.complete();
  }
}

interface PageHeaderPath {
  title?: string;
  link?: string[];
}
