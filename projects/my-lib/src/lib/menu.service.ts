import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { share } from 'rxjs/operators';
import { TssPlatformComponent } from './tss-platform.component';
import { Menu } from './tss-platform.interface';

@Injectable({ providedIn: 'root' })
export class MenuService implements OnDestroy {

    private _change$: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

    private _init$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    private _auth$: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);

    private data: any[] = [];

    platform: TssPlatformComponent;

    get change(): Observable<any[]> {
        return this._change$.pipe(share());
    }

    get menus() {
        return this.data;
    }

    init(menus: any[], platform: TssPlatformComponent, cb) {
        this.platform = platform;
        this.getLastNode(menus, url => {
            this.deep(1, menus, url, [], null, () => {
                this._init$.next(true);
                cb();
            });
        });
    }

    fullScreenCtrl(fullScreen: boolean) {
        this.platform.fullScreenCtrl({ fullScreen: fullScreen });
    }

    checkInit() {
        return this._init$;
    }

    setAuth(auth: string[]) {
        this._auth$.next(auth);
    }

    getAuth() {
        return this._auth$;
    }

    private deep(id: number, menus: Menu[], url: string, path: any[], parent: Menu, cb: CallableFunction) {
        for (const menu of menus) {
            if (typeof menu.children === 'undefined') {
                menu.children = [];
            }
            menu.__parent = parent;
            menu.id = id;
            if (menu['children'].length === 0) {
                this.data.push(path.concat([{ title: menu.title, url: menu.url, reuse: menu.reuse }]));
            }
            if (menu.url === url) {
                cb();
            } else {
                this.deep(id++, menu.children, url, path.concat([{ title: menu.title, url: menu.url }]), menu, cb);
            }
        }
    }

    getLastNode(obj, cb: CallableFunction) {
        const node = obj[obj.length - 1];
        if (typeof node.children === 'undefined') {
            node.children = [];
        }
        if (node.children.length === 0) {
            cb(node.url);
            return;
        }
        this.getLastNode(node.children, cb);
    }

    visit(data: Menu[], callback: (item: Menu, parentMenum: Menu | null, depth?: number) => void) {

        const inFn = (list: Menu[], parentMenu: Menu | null, depth: number) => {
            for (const item of list) {
                callback(item, parentMenu, depth);
                if (item.children && item.children.length > 0) {
                    inFn(item.children, item, depth + 1);
                } else {
                    item.children = [];
                }
            }
        };

        inFn(data, null, 0);
    }

    getPathByUrl(url: string): any[] {
        let ret: any[] = [];

        this.data.map(e => {
            e.map((x, i) => {
                if (x.url === url) {
                    ret = e.slice(0, i + 1);
                }
            });
        });

        return ret;
    }

    ngOnDestroy(): void {
        this._change$.unsubscribe();
    }
}
