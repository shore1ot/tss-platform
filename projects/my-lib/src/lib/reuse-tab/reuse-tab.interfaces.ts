import { ActivatedRouteSnapshot } from '@angular/router';

export interface ReuseTitle {
  text?: string;
}

export interface ReuseTabCached {
  title: ReuseTitle;

  url: string;

  _snapshot: ActivatedRouteSnapshot;

  _handle: any;
}

export declare interface OnReuseInit {
  _onReuseInit(): void;
}

export declare interface OnReuseDestroy {
  _onReuseDestroy(): void;
}
